terraform {
  backend "http" {}

  required_providers {
    nifcloud = {
      source  = "nifcloud/nifcloud"
      version = "1.10.1"
    }
  }
}

provider "nifcloud" {
  region = "jp-east-1"
}

resource "nifcloud_security_group" "this" {
  group_name        = "fw012"
  availability_zone = "east-12"
}

resource "nifcloud_security_group_rule" "this" {
  security_group_names = [nifcloud_security_group.this.group_name]
  type                 = "IN"
  from_port            = 80
  to_port              = 80
  protocol             = "TCP"
  cidr_ip              = "0.0.0.0/0"
}

resource "nifcloud_instance" "this" {
  instance_id       = "web012"
  availability_zone = "east-12"
  image_id          = 283 # Ubuntu Server 22.04 LTS
  key_name          = "meetup"
  instance_type     = "e-large"
  security_group    = nifcloud_security_group.this.group_name

  user_data = templatefile("${path.module}/templates/userdata.tftpl", {})

  network_interface {
    network_id = "net-COMMON_GLOBAL"
  }

  network_interface {
    network_id = "net-COMMON_PRIVATE"
  }
}

