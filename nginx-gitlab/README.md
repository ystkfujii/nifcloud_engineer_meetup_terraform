```bash
export NIFCLOUD_ACCESS_KEY_ID=<YOUR ACCESS_KEY_ID>
export NIFCLOUD_SECRET_ACCESS_KEY=<YOUR SECRET_ACCESS_KEY>
```

```console
fujii@ubuntu:/data/ghq/github.com/ystkfujii/nifcloud_engineer_meetup_terraform/01$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of nifcloud/nifcloud...
- Installing nifcloud/nifcloud v1.10.1...
- Installed nifcloud/nifcloud v1.10.1 (self-signed, key ID 3DC5DF4A206B4F8F)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```


```console
fujii@ubuntu:/data/ghq/github.com/ystkfujii/nifcloud_engineer_meetup_terraform/01$ terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # nifcloud_instance.this will be created
  + resource "nifcloud_instance" "this" {
      + accounting_type         = "2"
      + availability_zone       = "east-12"
      + disable_api_termination = false
      + id                      = (known after apply)
      + image_id                = "283"
      + instance_id             = "web001"
      + instance_state          = (known after apply)
      + instance_type           = "small"
      + key_name                = "terraform"
      + private_ip              = (known after apply)
      + public_ip               = (known after apply)
      + security_group          = "fw01"
      + unique_id               = (known after apply)

      + network_interface {
          + network_id                      = "net-COMMON_GLOBAL"
          + network_interface_attachment_id = (known after apply)
        }
      + network_interface {
          + network_id                      = "net-COMMON_PRIVATE"
          + network_interface_attachment_id = (known after apply)
        }
    }

  # nifcloud_security_group.this will be created
  + resource "nifcloud_security_group" "this" {
      + availability_zone      = "east-12"
      + group_name             = "fw01"
      + id                     = (known after apply)
      + log_limit              = 1000
      + revoke_rules_on_delete = true
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

nifcloud_security_group.this: Creating...
nifcloud_security_group.this: Still creating... [10s elapsed]
nifcloud_security_group.this: Still creating... [20s elapsed]
nifcloud_security_group.this: Creation complete after 24s [id=fw01]
nifcloud_instance.this: Creating...
nifcloud_instance.this: Still creating... [10s elapsed]
nifcloud_instance.this: Still creating... [20s elapsed]
nifcloud_instance.this: Still creating... [30s elapsed]
nifcloud_instance.this: Still creating... [40s elapsed]
nifcloud_instance.this: Still creating... [50s elapsed]
nifcloud_instance.this: Still creating... [1m0s elapsed]
nifcloud_instance.this: Still creating... [1m10s elapsed]
nifcloud_instance.this: Still creating... [1m20s elapsed]
nifcloud_instance.this: Still creating... [1m30s elapsed]
nifcloud_instance.this: Still creating... [1m40s elapsed]
nifcloud_instance.this: Still creating... [1m50s elapsed]
nifcloud_instance.this: Still creating... [2m0s elapsed]
nifcloud_instance.this: Still creating... [2m10s elapsed]
nifcloud_instance.this: Still creating... [2m20s elapsed]
nifcloud_instance.this: Still creating... [2m30s elapsed]
nifcloud_instance.this: Still creating... [2m40s elapsed]
nifcloud_instance.this: Still creating... [2m50s elapsed]
nifcloud_instance.this: Still creating... [3m0s elapsed]
nifcloud_instance.this: Still creating... [3m10s elapsed]
nifcloud_instance.this: Still creating... [3m20s elapsed]
nifcloud_instance.this: Still creating... [3m30s elapsed]
nifcloud_instance.this: Still creating... [3m40s elapsed]
nifcloud_instance.this: Still creating... [3m50s elapsed]
nifcloud_instance.this: Still creating... [4m0s elapsed]
nifcloud_instance.this: Still creating... [4m10s elapsed]
nifcloud_instance.this: Still creating... [4m20s elapsed]
nifcloud_instance.this: Still creating... [4m30s elapsed]
nifcloud_instance.this: Still creating... [4m40s elapsed]
nifcloud_instance.this: Still creating... [4m50s elapsed]
nifcloud_instance.this: Still creating... [5m0s elapsed]
nifcloud_instance.this: Still creating... [5m10s elapsed]
nifcloud_instance.this: Still creating... [5m20s elapsed]
nifcloud_instance.this: Still creating... [5m30s elapsed]
nifcloud_instance.this: Still creating... [5m40s elapsed]
nifcloud_instance.this: Still creating... [5m50s elapsed]
nifcloud_instance.this: Still creating... [6m0s elapsed]
nifcloud_instance.this: Still creating... [6m10s elapsed]
nifcloud_instance.this: Creation complete after 6m14s [id=web001]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```

```console
fujii@ubuntu:/data/ghq/github.com/ystkfujii/nifcloud_engineer_meetup_terraform/01$ terraform apply -destroy
nifcloud_security_group.this: Refreshing state... [id=fw01]
nifcloud_instance.this: Refreshing state... [id=web001]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # nifcloud_instance.this will be destroyed
  - resource "nifcloud_instance" "this" {
      - accounting_type         = "2" -> null
      - availability_zone       = "east-12" -> null
      - disable_api_termination = false -> null
      - id                      = "web001" -> null
      - image_id                = "283" -> null
      - instance_id             = "web001" -> null
      - instance_state          = "running" -> null
      - instance_type           = "small" -> null
      - key_name                = "terraform" -> null
      - private_ip              = "10.100.142.210" -> null
      - public_ip               = "222.158.239.146" -> null
      - security_group          = "fw01" -> null
      - unique_id               = "i-0wrjvtz8" -> null

      - network_interface {
          - network_id = "net-COMMON_GLOBAL" -> null
        }
      - network_interface {
          - network_id = "net-COMMON_PRIVATE" -> null
        }
    }

  # nifcloud_security_group.this will be destroyed
  - resource "nifcloud_security_group" "this" {
      - availability_zone      = "east-12" -> null
      - group_name             = "fw01" -> null
      - id                     = "fw01" -> null
      - log_limit              = 1000 -> null
      - revoke_rules_on_delete = true -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

nifcloud_instance.this: Destroying... [id=web001]
nifcloud_instance.this: Still destroying... [id=web001, 10s elapsed]
nifcloud_instance.this: Still destroying... [id=web001, 20s elapsed]
nifcloud_instance.this: Still destroying... [id=web001, 30s elapsed]
nifcloud_instance.this: Destruction complete after 39s
nifcloud_security_group.this: Destroying... [id=fw01]
nifcloud_security_group.this: Still destroying... [id=fw01, 10s elapsed]
nifcloud_security_group.this: Still destroying... [id=fw01, 20s elapsed]
nifcloud_security_group.this: Destruction complete after 23s

Apply complete! Resources: 0 added, 0 changed, 2 destroyed.
```
